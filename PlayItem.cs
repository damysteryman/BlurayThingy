﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class PlayItem : IbyteArraySerializable
    {
        public int SIZE => Length + 2;
        public UInt16 Length { get; set; }
        public string ClipInformationFileName { get; set; }
        public string ClipCodecIdentifier { get; set; }



        // 11 - reserved, 1 - IsMultiAngle, 4 - ConnectionCondition
        public UInt16 FEKJMFNEDKJFENFL { get; set; }
        public UInt16 RESERVED_1 { get; set; }
        public bool IsMultiAngle { get; set; }
        public byte ConnectionCondition { get; set; }



        public byte RefToSTCID { get; set; }
        public TimeSpan INTime { get; set; }
        public TimeSpan OUTTime { get; set; }

        
        public UOMaskTable UserOperations { get; set; }
        public bool PlayItemRandomAccessFlag { get; set; }
        public byte RESERVED_2 { get; set; } // 7 bits
        public byte StillMode { get; set; }
        public UInt16 StillTime { get; set; }
        //public byte NumberOfAngles { get; set; }
        public STNTable STNTable { get; set; }

        private static readonly Dictionary<string, int> LOC = new Dictionary<string, int>()
        {
            { "Length", 0x0 },
            { "ClipInformationFileName", 0x2 },
            { "ClipCodecIdentifier", 0x7 },
            { "FEKJMFNEDKJFENFL", 0xB },
            { "RefToSTCID", 0xD },
            { "INTime", 0xE },
            { "OUTTime", 0x12 },
            { "UserOperations", 0x16 },
            { "PlayItemRandomAccessFlag", 0x1E }, //bit 1, then other 7 bits are RESERVED_2
            { "StillMode", 0x1F },
            { "StillTime", 0x20 },
            { "STNTable", 0x22 }
        };

        public PlayItem(byte[] data)
        {
            Length = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Length"], 2, true), 0);
            ClipInformationFileName = data.GetByteSubArray(LOC["ClipInformationFileName"], 5).ToCharArray();
            ClipCodecIdentifier = data.GetByteSubArray(LOC["ClipCodecIdentifier"], 4).ToCharArray();

            FEKJMFNEDKJFENFL = BitConverter.ToUInt16(data.GetByteSubArray(LOC["FEKJMFNEDKJFENFL"], 2, true), 0);
            RESERVED_1 = (UInt16)(FEKJMFNEDKJFENFL >> 5);
            IsMultiAngle = ((FEKJMFNEDKJFENFL >> 4) & 1) == 1;
            ConnectionCondition = (byte)(FEKJMFNEDKJFENFL & 15);


            RefToSTCID = data[LOC["RefToSTCID"]];

            //INTime = new TimeSpan(0, 0, 0, 0, );
            INTime = TimeSpan.FromMilliseconds(BitConverter.ToUInt32(data.GetByteSubArray(LOC["INTime"], 4, true), 0) / 45);
            OUTTime = TimeSpan.FromMilliseconds(BitConverter.ToUInt32(data.GetByteSubArray(LOC["OUTTime"], 4, true), 0) / 45);
            UserOperations = new UOMaskTable(data.GetByteSubArray(LOC["UserOperations"], UOMaskTable.SIZE));
            PlayItemRandomAccessFlag = (data[LOC["PlayItemRandomAccessFlag"]] >> 7) == 1;
            RESERVED_2 = (byte)(data[LOC["PlayItemRandomAccessFlag"]] & 127);
            StillMode = data[LOC["StillMode"]];
            StillTime = BitConverter.ToUInt16(data.GetByteSubArray(LOC["StillTime"], 2, true), 0);

            int len = 2 + BitConverter.ToUInt16(data.GetByteSubArray(LOC["STNTable"], 2, true), 0);
            STNTable = new STNTable(data.GetByteSubArray(LOC["STNTable"], len));
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Length.GetBytes(true));
            result.AddRange(ClipInformationFileName.TobyteArray());
            result.AddRange(ClipCodecIdentifier.TobyteArray());

            UInt16 temp = (UInt16)((RESERVED_1 << 5) + ((IsMultiAngle ? 1 : 0) << 4) + ConnectionCondition);
            result.AddRange(temp.GetBytes(true));

            result.Add(RefToSTCID);
            result.AddRange(((UInt32)(INTime.TotalMilliseconds*45)).GetBytes(true));
            result.AddRange(((UInt32)(OUTTime.TotalMilliseconds * 45)).GetBytes(true));
            result.AddRange(UserOperations.ToRawData());
            result.Add((byte)(((PlayItemRandomAccessFlag ? 1 : 0) << 7) + RESERVED_2));
            result.Add(StillMode);
            result.AddRange(StillTime.GetBytes(true));
            result.AddRange(STNTable.ToRawData());

            return result.ToArray();
        }

        public override string ToString()
        {
            return ClipInformationFileName + "." + ClipCodecIdentifier;
        }
    }
}

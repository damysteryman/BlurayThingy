﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class ClipInfo : IbyteArraySerializable
    {
        public UInt32 Length { get; set; }
        public byte[] RESERVED_1 { get; set; }
        public byte ClipStreamType { get; set; }
        public byte ApplicationType { get; set; }
        public byte[] RESERVED_2 { get; set; }
        public bool IsCC5 { get; set; }
        public UInt32 TSRecordingRate { get; set; }
        public UInt32 NumberofSourcePackets { get; set; }
        public byte[] RESERVED_3 { get; set; }
        public byte[] TSTypeInfoBlock { get; set; }

        // following used only if IsCC5 = true
        public byte RESERVED_4 { get; set; }
        public byte FollowingClipStreamType { get; set; }
        public byte[] RESERVED_5 { get; set; }
        public string FollowingClipInformationFileName { get; set; }
        public byte FollowingClipCodecIdentifier { get; set; }
        public byte RESERVED_6 { get; set; }

        public ClipInfo(byte[] data)
        {
            throw new NotImplementedException();
        }

        public byte[] ToRawData()
        {
            throw new NotImplementedException();
        }
    }
}

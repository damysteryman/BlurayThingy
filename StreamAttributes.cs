﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class StreamAttributes : IbyteArraySerializable
    {
        public int SIZE => Length + 1;

        public byte Length { get; set; }
        public byte StreamCodingType { get; set; }

        public byte VideoFormat { get; set; }
        public byte FrameRate { get; set; }
        public byte AudioFormat { get; set; }
        public byte SampleRate { get; set; }
        public byte CharacterCode { get; set; }
        public string LanguageCode { get; set; }

        public StreamAttributes(byte[] data)
        {
            Length = data[0];
            StreamCodingType = data[1];

            switch (StreamCodingType)
            {
                case 0x02:
                case 0x1B:
                case 0xEA:
                    VideoFormat = data[2].GetNybbleHigh();
                    FrameRate = data[2].GetNybbleLow();
                    break;

                case 0x80:
                case 0x81:
                case 0x82:
                case 0x83:
                case 0x84:
                case 0x85:
                case 0x86:
                case 0xA1:
                case 0xA2:
                    AudioFormat = data[2].GetNybbleHigh();
                    SampleRate = data[2].GetNybbleLow();
                    LanguageCode = data.GetByteSubArray(3, 3).ToCharArray();
                    break;

                case 0x90:
                case 0x91:
                default:
                    LanguageCode = data.GetByteSubArray(2, 3).ToCharArray();
                    break;

                case 0x92:
                    CharacterCode = data[2];
                    LanguageCode = data.GetByteSubArray(3, 3).ToCharArray();
                    break;
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.Add(Length);
            result.Add(StreamCodingType);

            switch (StreamCodingType)
            {
                case 0x02:
                case 0x1B:
                case 0xEA:
                    result.Add((byte)((VideoFormat << 4) + FrameRate));
                    break;

                case 0x80:
                case 0x81:
                case 0x82:
                case 0x83:
                case 0x84:
                case 0x85:
                case 0x86:
                case 0xA1:
                case 0xA2:
                    result.Add((byte)((AudioFormat << 4) + SampleRate));
                    result.AddRange(LanguageCode.TobyteArray());
                    break;

                case 0x90:
                case 0x91:
                default:
                    result.AddRange(LanguageCode.TobyteArray());
                    break;

                case 0x92:
                    result.Add(CharacterCode);
                    result.AddRange(LanguageCode.TobyteArray());
                    break;
            }

            // pad out to correct size
            while (result.Count < SIZE)
                result.Add(0);

            return result.ToArray();
        }
    }
}

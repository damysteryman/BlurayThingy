﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class UOMaskTable : IbyteArraySerializable
    {
        public const int SIZE = 8;
        private BitArray _flags;
        private byte[] _bytes;

        public bool MenuCall
        {
            get => _flags[0];
            set => _flags[0] = value;
        }
        public bool TitleSearch
        {
            get => _flags[1];
            set => _flags[1] = value;
        }
        public bool ChapterSearch
        {
            get => _flags[2];
            set => _flags[2] = value;
        }
        public bool TimeSearch
        {
            get => _flags[3];
            set => _flags[3] = value;
        }
        public bool SkipToNextPoint
        {
            get => _flags[4];
            set => _flags[4] = value;
        }
        public bool SkipToPrevPoint
        {
            get => _flags[5];
            set => _flags[5] = value;
        }
        public bool RESERVED_1
        {
            get => _flags[6];
            set => _flags[6] = value;
        }
        public bool Stop
        {
            get => _flags[7];
            set => _flags[7] = value;
        }
        public bool PauseOn
        {
            get => _flags[8];
            set => _flags[8] = value;
        }
        public bool RESERVED_2
        {
            get => _flags[9];
            set => _flags[9] = value;
        }
        public bool StillOff
        {
            get => _flags[10];
            set => _flags[10] = value;
        }
        public bool ForwardPlay
        {
            get => _flags[11];
            set => _flags[11] = value;
        }
        public bool BackwardPlay
        {
            get => _flags[12];
            set => _flags[12] = value;
        }
        public bool Resume
        {
            get => _flags[13];
            set => _flags[13] = value;
        }
        public bool MoveUpSelectedButton
        {
            get => _flags[14];
            set => _flags[14] = value;
        }
        public bool MoveDownSelectedButton
        {
            get => _flags[15];
            set => _flags[15] = value;
        }
        public bool MoveLeftSelectedButton
        {
            get => _flags[16];
            set => _flags[16] = value;
        }
        public bool MoveRightSelectedButton
        {
            get => _flags[17];
            set => _flags[17] = value;
        }
        public bool SelectButton
        {
            get => _flags[18];
            set => _flags[18] = value;
        }
        public bool ActivateButton
        {
            get => _flags[19];
            set => _flags[19] = value;
        }
        public bool SelectAndActivateButton
        {
            get => _flags[20];
            set => _flags[20] = value;
        }
        public bool PrimaryAudioStreamNumberChange
        {
            get => _flags[21];
            set => _flags[21] = value;
        }
        public bool RESERVED_3
        {
            get => _flags[22];
            set => _flags[22] = value;
        }
        public bool AngleNumberChange
        {
            get => _flags[23];
            set => _flags[23] = value;
        }
        public bool PopupOn
        {
            get => _flags[24];
            set => _flags[24] = value;
        }
        public bool PopupOff
        {
            get => _flags[25];
            set => _flags[25] = value;
        }
        public bool PrimaryPGEnableDisable
        {
            get => _flags[26];
            set => _flags[26] = value;
        }
        public bool PrimaryPGStreamNumberChange
        {
            get => _flags[27];
            set => _flags[27] = value;
        }
        public bool SecondaryVideoEnableDisable
        {
            get => _flags[28];
            set => _flags[28] = value;
        }
        public bool SecondaryVideoStreamNumberChange
        {
            get => _flags[29];
            set => _flags[29] = value;
        }
        public bool SecondaryAudioEnableDisable
        {
            get => _flags[30];
            set => _flags[30] = value;
        }
        public bool SecondaryAudioStreamNumberChange
        {
            get => _flags[31];
            set => _flags[31] = value;
        }
        public bool RESERVED_4
        {
            get => _RESERVED_5[0];
            set => _RESERVED_5[0] = value;
        }
        public bool SecondaryPGStreamNumberChange
        {
            get => _RESERVED_5[1];
            set => _RESERVED_5[1] = value;
        }
        private BitArray _RESERVED_5 { get; set; }

        public UOMaskTable(byte[] data)
        {
            _flags = new BitArray(data.GetByteSubArray(0, 4));
            _RESERVED_5 = new BitArray(data.GetByteSubArray(4, 4));
            _bytes = data;
        }

        public byte[] ToRawData()
        {
            return _bytes;
        }
    }
}

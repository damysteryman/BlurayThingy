﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class AppInfoPlayList : IbyteArraySerializable
    {
        public UInt32 SIZE => Length + 4;
        public UInt32 Length { get; set; }
        public byte RESERVED_1 { get; set; }
        public byte PlaybackType { get; set; }
        public UInt16 PlaybackCount { get; set; }
        public UOMaskTable UserOperations { get; set; }
        public bool RandomAccessFlag { get; set; }
        public bool AudioMixFlag { get; set; }
        public bool LosslessBypassFlag { get; set; }
        public bool[] RESERVED_2 { get; set; }

        private byte[] _flagData { get; set; }

        public AppInfoPlayList(byte[] data)
        {
            Length = BitConverter.ToUInt32(data.GetByteSubArray(0, 4, true), 0);
            RESERVED_1 = data[4];
            PlaybackType = data[5];
            PlaybackCount = BitConverter.ToUInt16(data.GetByteSubArray(6, 2, true), 0);
            UserOperations = new UOMaskTable(data.GetByteSubArray(8, UOMaskTable.SIZE));
            _flagData = data.GetByteSubArray(0x10, 2);
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Length.GetBytes(true));
            result.Add(RESERVED_1);
            result.Add(PlaybackType);
            result.AddRange(PlaybackCount.GetBytes(true));
            result.AddRange(UserOperations.ToRawData());
            result.AddRange(_flagData);

            return result.ToArray();
        }
    }
}

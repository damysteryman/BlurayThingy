﻿using System;
using System.Collections.Generic;

namespace BlurayThingy
{
    public static class Utils
    {
        public static byte[] GetByteSubArray(this byte[] data, int startLoc, int length, bool bigEndian = false, int padbytes = 0)
        {
            byte[] value = new byte[length < padbytes ? padbytes : length];

            int offset = padbytes - length;
            offset = offset > 0 ? offset : 0;
            for (int i = 0; i < offset; i++)
                value[i] = 0;
            for (int i = 0; i < length; i++)
                value[offset + i] = data[startLoc + i];

            if (BitConverter.IsLittleEndian)
            {
                if (bigEndian)
                {
                    Array.Reverse(value);
                }
            }
            else
            {
                if (!bigEndian)
                {
                    Array.Reverse(value);
                }
            }

            return value;
        }

        public static byte[] GetBytes(this ushort value, bool bigEndian = false)
        {
            byte[] result = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
                if (bigEndian)
                    Array.Reverse(result);
                else
                if (!bigEndian)
                    Array.Reverse(result);

            return result;
        }

        public static byte[] GetBytes(this uint value, bool bigEndian = false)
        {
            byte[] result = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
                if (bigEndian)
                    Array.Reverse(result);
            else
                if (!bigEndian)
                    Array.Reverse(result);

            return result;
        }

        public static byte Sanitizebyte(this byte value)
        {
            if (value < 0 || value > byte.MaxValue)
                return 0;
            else
                return value;
        }

        public static byte Sanitizebyte(this int value)
        {
            if (value < 0 || value > byte.MaxValue)
                return 0;
            else
                return (byte)value;
        }

        public static UInt16 SanitizeUInt16(this UInt16 value)
        {
            if (value < 0 || value > UInt16.MaxValue)
                return 0;
            else
                return value;
        }

        public static UInt16 SanitizeUInt16(this int value)
        {
            if (value < 0 || value > UInt16.MaxValue)
                return 0;
            else
                return (UInt16)value;
        }

        public static Int32 SanitizeInt32(this Int32 value)
        {
            if (value < 0 || value > Int32.MaxValue)
                return 0;
            else
                return value;
        }

        public static UInt32 SanitizeUInt32(this UInt32 value)
        {
            if (value < 0 || value > UInt32.MaxValue)
                return 0;
            else
                return value;
        }

        public static UInt64 SanitizeUInt16(this UInt64 value)
        {
            if (value < 0 || value > UInt64.MaxValue)
                return 0;
            else
                return value;
        }

        public static byte GetNybbleHigh(this byte value)
        {
            return (byte)(value >> 4);
        }

        public static byte GetNybbleLow(this byte value)
        {
            return (byte)(value & 15);
        }

        public static byte[] TobyteArray(this string str)
        {
            List<byte> result = new List<byte>();

            foreach (char c in str)
                result.Add((byte)c);

            return result.ToArray();
        }

        public static string ToCharArray(this byte[] bytes)
        {
            string result = "";

            foreach (byte b in bytes)
                result += (char)b;

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class SubPlayItem : IbyteArraySerializable
    {
        public int SIZE => Length + 2;
        public UInt16 Length { get; set; }
        public string ClipInformationFileName { get; set; }
        public string ClipCodecIdentifier { get; set; }
        public UInt32 RESERVED_1 { get; set; }
        public byte ConnectionCondition { get; set; }
        public bool IsMultiClipEntries { get; set; }
        public byte RefToSTCID { get; set; }
        public UInt32 INTime { get; set; }
        public UInt32 OUTTime { get; set; }
        public UInt16 SyncPlayItemID { get; set; }
        public UInt32 SyncStartPTS { get; set; }
        public byte NumberOfMultiClipEntries { get; set; }
        public byte RESERVED_2 { get; set; }
        public List<SubItemMultiClipEntry> MultiClipEntries { get; set; }

        public SubPlayItem(byte[] data)
        {
            Length = BitConverter.ToUInt16(data.GetByteSubArray(0x0, 2, true), 0);
            ClipInformationFileName = data.GetByteSubArray(0x2, 5).ToCharArray();
            ClipCodecIdentifier = data.GetByteSubArray(0x7, 4).ToCharArray();

            UInt32 tempReserved = BitConverter.ToUInt32(data.GetByteSubArray(0xB, 4, true), 0);
            RESERVED_1 = tempReserved >> 5;
            ConnectionCondition = (byte)((tempReserved >> 1) & 15);
            IsMultiClipEntries = (tempReserved & 1) == 1;

            RefToSTCID = data[0xF];
            INTime = BitConverter.ToUInt32(data.GetByteSubArray(0x10, 4, true), 0);
            OUTTime = BitConverter.ToUInt32(data.GetByteSubArray(0x14, 4, true), 0);
            SyncPlayItemID = BitConverter.ToUInt16(data.GetByteSubArray(0x18, 2, true), 0);
            SyncStartPTS = BitConverter.ToUInt32(data.GetByteSubArray(0x1A, 4, true), 0);

            if (IsMultiClipEntries)
            {
                NumberOfMultiClipEntries = data[0x1B];
                RESERVED_2 = data[0x1C];

                MultiClipEntries = new List<SubItemMultiClipEntry>();
                for (int i = 0; i < NumberOfMultiClipEntries; i++)
                    MultiClipEntries.Add(new SubItemMultiClipEntry(data.GetByteSubArray(0x1D + (i * SubItemMultiClipEntry.SIZE), SubItemMultiClipEntry.SIZE)));
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Length.GetBytes(true));
            result.AddRange(ClipInformationFileName.TobyteArray());
            result.AddRange(ClipCodecIdentifier.TobyteArray());
            UInt32 temp = (UInt32)((RESERVED_1 << 5) + (ConnectionCondition << 1) + (IsMultiClipEntries ? 1 : 0));
            result.AddRange(temp.GetBytes(true));
            result.Add(RefToSTCID);
            result.AddRange(INTime.GetBytes(true));
            result.AddRange(OUTTime.GetBytes(true));
            result.AddRange(SyncPlayItemID.GetBytes(true));
            result.AddRange(SyncStartPTS.GetBytes(true));

            if (IsMultiClipEntries)
            {
                result.Add(NumberOfMultiClipEntries);
                result.Add(RESERVED_2);

                foreach (SubItemMultiClipEntry mce in MultiClipEntries)
                    result.AddRange(mce.ToRawData());
            }

            return result.ToArray();
        }
    }

    public class SubItemMultiClipEntry : IbyteArraySerializable
    {
        public const int SIZE = 10;
        public string ClipInformationFileName { get; set; }
        public string ClipCodecIdentifier { get; set; }
        public byte RefToSTCID { get; set; }

        public SubItemMultiClipEntry(byte[] data)
        {
            ClipInformationFileName = data.GetByteSubArray(0x0, 5).ToCharArray();
            ClipCodecIdentifier = data.GetByteSubArray(0x5, 4).ToCharArray();
            RefToSTCID = data[0x9];
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(ClipInformationFileName.TobyteArray());
            result.AddRange(ClipCodecIdentifier.TobyteArray());
            result.Add(RefToSTCID);

            return result.ToArray();
        }
    }
}

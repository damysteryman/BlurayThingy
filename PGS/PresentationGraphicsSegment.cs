using System;
using System.Collections.Generic;

namespace BlurayThingy.PGS
{
    public class PresentationGraphicsSegment : IbyteArraySerializable
    {
        public const int HEADER_SIZE = 13;
        public enum TYPE
        {
            PCS = 0x16,
            WDS = 0x17,
            PDS = 0x14,
            ODS = 0x15,
            END = 0x80,
            //END2 = 0xFD
        }

        public string Magic { get; private set; }
        public uint PTS { get; set; }
        public uint DTS { get; set; }
        public TYPE Type { get; set; }
        public ushort Size { get; set; }
        private static readonly Dictionary<string, int> LOC = new Dictionary<string, int>()
        {
            { "Magic", 0x0 },
            { "PTS", 0x2 },
            { "DTS", 0x6 },
            { "Type", 0xA },
            { "Size", 0xB }
        };
        public PresentationGraphicsSegment(byte[] data)
        {
            Magic = System.Text.Encoding.ASCII.GetString(data, LOC["Magic"], 2);
            PTS = BitConverter.ToUInt32(data.GetByteSubArray(LOC["PTS"], 4, true), 0);
            DTS = BitConverter.ToUInt32(data.GetByteSubArray(LOC["DTS"], 4, true), 0);
            Type = (TYPE)data[LOC["Type"]];
            Size = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Size"], 2, true), 0);
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(System.Text.Encoding.ASCII.GetBytes(Magic));
            result.AddRange(Utils.GetBytes(PTS, true));
            result.AddRange(Utils.GetBytes(DTS, true));
            result.Add((byte)Type);
            result.AddRange(Utils.GetBytes(Size, true));

            return result.ToArray();
        }

        public override string ToString()
        {
            return Type.ToString();
        }
    }
}
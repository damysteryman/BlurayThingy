using System;
using System.Collections.Generic;
using System.IO;

namespace BlurayThingy.PGS
{
    public static class PGSSerialization
    {
        public static List<PresentationGraphicsSegment> Deserialize(string filePath)
        {
            List<PresentationGraphicsSegment> segments = new List<PresentationGraphicsSegment>();
            using (FileStream fs = File.Open(filePath, FileMode.Open))
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] currentByte = new byte[2];
                    while (br.Read(currentByte, 0, 2) > 0)
                    {
                        if (currentByte[0] == 'P' && currentByte[1] == 'G')
                        {
                            byte[] header = new byte[13];
                            header[0] = (byte)'P';
                            header[1] = (byte)'G';
                            br.Read(header, 2, header.Length - 2);
                            uint size = BitConverter.ToUInt16(header.GetByteSubArray(11, 2, true));
                            byte[] data = new byte[header.Length + size];
                            header.CopyTo(data, 0);
                            br.Read(data, header.Length, (int)size);
                            segments.Add(MakeSegment(data));
                        }
                    }
                } 

            return segments;      
        }

        public static byte[] Serialize(List<PresentationGraphicsSegment> segments)
        {
            List<byte> output = new List<byte>();
            foreach (IbyteArraySerializable pgs in segments)
                output.AddRange(pgs.ToRawData());

            return output.ToArray();
        }

        private static PresentationGraphicsSegment MakeSegment(byte[] data)
        {
            switch((PresentationGraphicsSegment.TYPE)data[0xA])
            {
                case PresentationGraphicsSegment.TYPE.PCS:
                    return new PresentationCompositionSegment(data);

                case PresentationGraphicsSegment.TYPE.WDS:
                    return new WindowDefinitionSegment(data);

                case PresentationGraphicsSegment.TYPE.PDS:
                    return new PaletteDefinitionSegment(data);

                case PresentationGraphicsSegment.TYPE.ODS:
                    return new ObjectDefinitionSegment(data);

                case PresentationGraphicsSegment.TYPE.END:
                default:
                    return new PresentationGraphicsSegment(data);
            }
        }
    }

    
}

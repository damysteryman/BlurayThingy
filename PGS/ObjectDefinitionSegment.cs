using System;
using System.Collections.Generic;

namespace BlurayThingy.PGS
{
    public class ObjectDefinitionSegment : PresentationGraphicsSegment, IbyteArraySerializable
    {
        public ushort ID { get; set; }
        public byte Version { get; set; }
        public byte LastInSequenceFlag { get; set; }
        public uint DataLength { get; set; }
        public ushort Width { get; set; }
        public ushort Height { get; set; }
        public byte[] Data { get; set; }
        public static readonly Dictionary<string, int> LOC = new Dictionary<string, int>()
        {
            { "ID", 0x0D },
            { "Version", 0x0F },
            { "LastInSequenceFlag", 0x10 },
            { "DataLength", 0x11 },
            { "Width", 0x14 },
            { "Height", 0x16 },
            { "Data", 0x18 },
        };

        public ObjectDefinitionSegment(byte[] data) : base(data)
        {
            ID = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ID"], 2, true), 0);
            Version = data[LOC["Version"]];
            LastInSequenceFlag = data[LOC["LastInSequenceFlag"]];
            byte[] derp = data.GetByteSubArray(LOC["DataLength"], 3, true, 4);
            // DataLength can be longer than Segment Size, since 1 ObjectDefinition can span multiple Segments
            DataLength = BitConverter.ToUInt32(data.GetByteSubArray(LOC["DataLength"], 3, true, 4), 0) - 4;
            Width = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Width"], 2, true), 0);
            Height = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Height"], 2, true), 0);
            Data = data.GetByteSubArray(LOC["Data"], data.Length - LOC["Data"]);
        }

        public new byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(base.ToRawData());
            result.AddRange(Utils.GetBytes(ID, true));
            result.Add(Version);
            result.Add(LastInSequenceFlag);
            byte[] datalength = BitConverter.GetBytes(DataLength + 4);
            result.AddRange(datalength.GetByteSubArray(0, 3, true));
            result.AddRange(Utils.GetBytes(Width, true));
            result.AddRange(Utils.GetBytes(Height, true));
            result.AddRange(Data);

            return result.ToArray();
        }
    }
}
using System;
using System.Collections.Generic;

namespace BlurayThingy.PGS
{
    public class WindowDefinitionSegment : PresentationGraphicsSegment, IbyteArraySerializable
    {
        public byte WindowsCount { get; set; }
        public byte ID { get; set; }
        public ushort HorizontalPosition { get; set; }
        public ushort VerticalPosition { get; set; }
        public ushort Width { get; set; }
        public ushort Height { get; set; }
        public byte[] UNK { get; set; }
        private static readonly Dictionary<string, int> LOC = new Dictionary<string, int>()
        {
            { "WindowsCount", 0x0D },
            { "ID", 0x0E },
            { "HorizontalPosition", 0x0F },
            { "VerticalPosition", 0x11 },
            { "Width", 0x13 },
            { "Height", 0x15 },
            { "UNK", 0x17 },
        };

        public WindowDefinitionSegment(byte[] data) : base(data)
        {
            WindowsCount = data[LOC["WindowsCount"]];
            ID = data[LOC["ID"]];
            HorizontalPosition = BitConverter.ToUInt16(data.GetByteSubArray(LOC["HorizontalPosition"], 2, true), 0);
            VerticalPosition = BitConverter.ToUInt16(data.GetByteSubArray(LOC["VerticalPosition"], 2, true), 0);
            Width = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Width"], 2, true), 0);
            Height = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Height"], 2, true), 0);
            UNK = data.GetByteSubArray(LOC["UNK"], data.Length - LOC["UNK"]);
        }

        public new byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(base.ToRawData());
            result.Add(WindowsCount);
            result.Add(ID);
            result.AddRange(Utils.GetBytes(HorizontalPosition, true));
            result.AddRange(Utils.GetBytes(VerticalPosition, true));
            result.AddRange(Utils.GetBytes(Width, true));
            result.AddRange(Utils.GetBytes(Height, true));
            result.AddRange(UNK);

            return result.ToArray();
        }
    }
}
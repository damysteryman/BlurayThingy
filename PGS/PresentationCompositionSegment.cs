using System;
using System.Collections.Generic;

namespace BlurayThingy.PGS
{
    public class PresentationCompositionSegment : PresentationGraphicsSegment, IbyteArraySerializable
    {
        public ushort Width { get; set; }
        public ushort Height { get; set; }
        public byte FrameRate { get; set; }
        public ushort CompositionNumber { get; set;}
        public byte CompositionState { get; set; }
        public byte PaletteUpdateFlag { get; set; }
        public byte PaletteID { get; set; }
        public byte CompositionObjectsCount { get; set; }
        public List<CompositionObject> CompositionObjects { get; set; }
        private static readonly Dictionary<string, int> LOC = new Dictionary<string, int>()
        {
            { "Width", 0x0D },
            { "Height", 0x0F },
            { "FrameRate", 0x11 },
            { "CompositionNumber", 0x12 },
            { "CompositionState", 0x14 },
            { "PaletteUpdateFlag", 0x15 },
            { "PaletteID", 0x16 },
            { "CompositionObjectsCount", 0x17 },
            { "CompositionObjects", 0x18 },
        };

        public PresentationCompositionSegment(byte[] data) : base(data)
        {
            Width = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Width"], 2, true), 0);
            Height = BitConverter.ToUInt16(data.GetByteSubArray(LOC["Height"], 2, true), 0);
            FrameRate = data[LOC["FrameRate"]];
            CompositionNumber = BitConverter.ToUInt16(data.GetByteSubArray(LOC["CompositionNumber"], 2, true), 0);
            CompositionState = data[LOC["CompositionState"]];
            PaletteUpdateFlag = data[LOC["PaletteUpdateFlag"]];
            PaletteID = data[LOC["PaletteID"]];
            CompositionObjectsCount = data[LOC["CompositionObjectsCount"]];
            CompositionObjects = new List<CompositionObject>();

            int pos = 0;
            for (int i = 0; i < CompositionObjectsCount; i++)
            {
                int size = data[LOC["CompositionObjects"] + pos + CompositionObject.LOC["ObjectCroppedFlag"]] == 0x40 ? 16 : 8;
                CompositionObjects.Add(new CompositionObject(data.GetByteSubArray(LOC["CompositionObjects"] + pos, size)));
                pos += size;
            }
        }

        public new byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(base.ToRawData());
            result.AddRange(Utils.GetBytes(Width, true));
            result.AddRange(Utils.GetBytes(Height, true));
            result.Add(FrameRate);
            result.AddRange(Utils.GetBytes(CompositionNumber, true));
            result.Add(CompositionState);
            result.Add(PaletteUpdateFlag);
            result.Add(PaletteID);
            result.Add(CompositionObjectsCount);
            foreach (CompositionObject co in CompositionObjects)
                result.AddRange(co.ToRawData());

            return result.ToArray();
        }
    }

    public class CompositionObject : IbyteArraySerializable
    {
        public ushort ObjectID { get; set; }
        public byte WindowID { get; set; }
        public byte ObjectCroppedFlag { get; set; }
        public ushort ObjectHorizontalPosition { get; set; }
        public ushort ObjectVerticalPosition { get; set; }
        public ushort ObjectCroppingHorizontalPosition { get; set; }
        public ushort ObjectCroppingVerticalPosition { get; set; }
        public ushort ObjectCroppingWidth { get; set; }
        public ushort ObjectCroppingHeight { get; set; }

        public static readonly Dictionary<string, int> LOC = new Dictionary<string, int>()
        {
            { "ObjectID", 0x0 },
            { "WindowID", 0x2 },
            { "ObjectCroppedFlag", 0x3 },
            { "ObjectHorizontalPosition", 0x4 },
            { "ObjectVerticalPosition", 0x6 },
            { "ObjectCroppingHorizontalPosition", 0x8 },
            { "ObjectCroppingVerticalPosition", 0xA },
            { "ObjectCroppingWidth", 0xC },
            { "ObjectCroppingHeight", 0xE },
        };

        public CompositionObject(byte[] data)
        {
            ObjectID = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ObjectID"], 2, true), 0);
            WindowID = data[LOC["WindowID"]];
            ObjectCroppedFlag = data[LOC["ObjectCroppedFlag"]];
            ObjectHorizontalPosition = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ObjectHorizontalPosition"], 2, true), 0);
            ObjectVerticalPosition = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ObjectVerticalPosition"], 2, true), 0);
            
            if (data.Length > 8)
            {
                ObjectCroppingHorizontalPosition = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ObjectCroppingHorizontalPosition"], 2, true), 0);
                ObjectCroppingVerticalPosition = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ObjectCroppingVerticalPosition"], 2, true), 0);
                ObjectCroppingWidth = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ObjectCroppingWidth"], 2, true), 0);
                ObjectCroppingHeight = BitConverter.ToUInt16(data.GetByteSubArray(LOC["ObjectCroppingHeight"], 2, true), 0);
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Utils.GetBytes(ObjectID, true));
            result.Add(WindowID);
            result.Add(ObjectCroppedFlag);
            result.AddRange(Utils.GetBytes(ObjectHorizontalPosition, true));
            result.AddRange(Utils.GetBytes(ObjectVerticalPosition, true));

            if (ObjectCroppedFlag != 0)
            {
                result.AddRange(Utils.GetBytes(ObjectCroppingHorizontalPosition, true));
                result.AddRange(Utils.GetBytes(ObjectCroppingVerticalPosition, true));
                result.AddRange(Utils.GetBytes(ObjectCroppingWidth, true));
                result.AddRange(Utils.GetBytes(ObjectCroppingHeight, true));
            }

            return result.ToArray();
        }
    }
}
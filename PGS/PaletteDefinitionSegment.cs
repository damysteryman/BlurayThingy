using System;
using System.Collections.Generic;
using System.Drawing;

namespace BlurayThingy.PGS
{
    public class PaletteDefinitionSegment : PresentationGraphicsSegment, IbyteArraySerializable
    {
        public byte ID { get; set; }
        public byte Version { get; set; }
        public byte EntryID { get; set; }
        public List<PaletteEntry> PaletteEntries { get; set; }
        private static readonly Dictionary<string, int> LOC = new Dictionary<string, int>()
        {
            { "ID", 0x0D },
            { "Version", 0x0E },
            { "FirstEntry", 0x0F }
        };

        public PaletteDefinitionSegment(byte[] data) : base(data)
        {
            ID = data[LOC["ID"]];
            Version = data[LOC["Version"]];
            PaletteEntries = new List<PaletteEntry>();
            int count = (Size - 2) / 5;
            for (int i = 0; i < count; i++)
            {
                PaletteEntries.Add(new PaletteEntry(data.GetByteSubArray(LOC["FirstEntry"] + (5 * i), 5)));
            }
        }

        public new byte[] ToRawData()
        {
            List<byte> result = new List<byte>();
            
            result.AddRange(base.ToRawData());
            result.Add(ID);
            result.Add(Version);
            foreach (PaletteEntry pe in PaletteEntries)
                result.AddRange(pe.ToRawData());

            return result.ToArray();
        }
    }

    public class PaletteEntry : IbyteArraySerializable
    {
        public byte EntryID { get; set; }
        public byte Y { get; set; }
        public byte Cb { get; set; }
        public byte Cr { get; set; }
        public byte A { get; set; }
        public Color PaletteColor { get; set; }
        public bool Modified { get; set; }

        public PaletteEntry(byte[] data)
        {
            EntryID = data[0x0];
            Y = data[0x1];
            Cb = data[0x3];
            Cr = data[0x2];
            A = data[0x4];

            int[] _rgb = YCbCr2Rgb(Y, Cb, Cr, false);
            PaletteColor = Color.FromArgb(A, _rgb[0], _rgb[1],　_rgb[2]);
            Modified = false;
        }

        /**
         * Adapted from https://github.com/SubtitleEdit/subtitleedit/blob/master/src/libse/BluRaySup/BluRaySupPalette.cs
         * Convert YCBCr color info to RGB
         * @param y  8 bit luminance
         * @param cb 8 bit chrominance blue
         * @param cr 8 bit chrominance red
         * @return Integer array with red, blue, green component (in this order)
         */
        public static int[] YCbCr2Rgb(int y, int cb, int cr, bool useBt601)
        {
            int[] rgb = new int[3];
            double r, g, b;

            y -= 16;
            cb -= 128;
            cr -= 128;

            var y1 = y * 1.164383562;
            if (useBt601)
            {
                /* BT.601 for YCbCr 16..235 -> RGB 0..255 (PC) */
                r = y1 + cr * 1.596026317;
                g = y1 - cr * 0.8129674985 - cb * 0.3917615979;
                b = y1 + cb * 2.017232218;
            }
            else
            {
                /* BT.709 for YCbCr 16..235 -> RGB 0..255 (PC) */
                r = y1 + cr * 1.792741071;
                g = y1 - cr * 0.5329093286 - cb * 0.2132486143;
                b = y1 + cb * 2.112401786;
            }
            rgb[0] = (int)Math.Clamp(Math.Round(r), 0, 255);
            rgb[1] = (int)Math.Clamp(Math.Round(g), 0, 255);
            rgb[2] = (int)Math.Clamp(Math.Round(b), 0, 255);
            return rgb;
        }

        /**
         * Adapted from https://github.com/SubtitleEdit/subtitleedit/blob/master/src/libse/BluRaySup/BluRaySupPalette.cs
         * Convert RGB color info to YCBCr
         * @param r 8 bit red component
         * @param g 8 bit green component
         * @param b 8 bit blue component
         * @return Integer array with luminance (Y), chrominance blue (Cb), chrominance red (Cr) (in this order)
         */
        public static int[] Rgb2YCbCr(int r, int g, int b, bool useBt601)
        {
            int[] yCbCr = new int[3];
            double y, cb, cr;

            if (useBt601)
            {
                /* BT.601 for RGB 0..255 (PC) -> YCbCr 16..235 */
                y = r * 0.299 * 219 / 255 + g * 0.587 * 219 / 255 + b * 0.114 * 219 / 255;
                cb = -r * 0.168736 * 224 / 255 - g * 0.331264 * 224 / 255 + b * 0.5 * 224 / 255;
                cr = r * 0.5 * 224 / 255 - g * 0.418688 * 224 / 255 - b * 0.081312 * 224 / 255;
            }
            else
            {
                /* BT.709 for RGB 0..255 (PC) -> YCbCr 16..235 */
                y = r * 0.2126 * 219 / 255 + g * 0.7152 * 219 / 255 + b * 0.0722 * 219 / 255;
                cb = -r * 0.2126 / 1.8556 * 224 / 255 - g * 0.7152 / 1.8556 * 224 / 255 + b * 0.5 * 224 / 255;
                cr = r * 0.5 * 224 / 255 - g * 0.7152 / 1.5748 * 224 / 255 - b * 0.0722 / 1.5748 * 224 / 255;
            }
            yCbCr[0] = (int)Math.Clamp(Math.Round(16 + y), 16, 235);
            yCbCr[1] = (int)Math.Clamp(Math.Round(128 + cb), 16, 240);
            yCbCr[2] = (int)Math.Clamp(Math.Round(128 + cr), 16, 240);
            return yCbCr;
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();
            
            result.Add(EntryID);

            if (Modified)
            {
                int[] ycbcr = Rgb2YCbCr(PaletteColor.R, PaletteColor.G, PaletteColor.B, false);
                result.Add((byte)ycbcr[0]);
                result.Add((byte)ycbcr[2]);
                result.Add((byte)ycbcr[1]);
            }
            else
            {
                result.Add(Y);
                result.Add(Cr);
                result.Add(Cb);
            }
            result.Add(A);

            return result.ToArray();
        }
    }
}
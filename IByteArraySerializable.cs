﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public interface IbyteArraySerializable
    {
        byte[] ToRawData();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class Mpls : IbyteArraySerializable
    {
        public string TypeIndicator { get; set; }
        public string VersionNumber { get; set; }
        public UInt32 PlayListStartAddress { get; set; }
        public UInt32 PlayListMarkStartAddress { get; set; }
        public UInt32 ExtensionDataStartAddress { get; set; }
        public byte[] RESERVED_1 { get; set; }
        public AppInfoPlayList AppInfoPlayList { get; set; }
        public PlayList PlayList { get; set; }
        public PlayListMark PlayListMark { get; set; }
        public ExtensionData ExtensionData { get; set; }

        public Mpls(byte[] data)
        {
            TypeIndicator = data.GetByteSubArray(0x0, 4).ToCharArray();
            VersionNumber = data.GetByteSubArray(0x4, 4).ToCharArray();
            PlayListStartAddress = BitConverter.ToUInt32(data.GetByteSubArray(0x8, 4, true), 0);
            PlayListMarkStartAddress = BitConverter.ToUInt32(data.GetByteSubArray(0xC, 4, true), 0);
            ExtensionDataStartAddress = BitConverter.ToUInt32(data.GetByteSubArray(0x10, 4, true), 0);
            RESERVED_1 = data.GetByteSubArray(0x14, 20);

            int offset = 0x28;
            int len = 4 + BitConverter.ToInt32(data.GetByteSubArray(offset, 4, true), 0);
            AppInfoPlayList = new AppInfoPlayList(data.GetByteSubArray(offset, len));

            len = 4 + BitConverter.ToInt32(data.GetByteSubArray((int)PlayListStartAddress, 4, true), 0);
            PlayList = new PlayList(data.GetByteSubArray((int)PlayListStartAddress, len));

            len = 4 + BitConverter.ToInt32(data.GetByteSubArray((int)PlayListMarkStartAddress, 4, true), 0);
            PlayListMark = new PlayListMark(data.GetByteSubArray((int)PlayListMarkStartAddress, len));

            if (ExtensionDataStartAddress != 0)
            {
                len = 4 + BitConverter.ToInt32(data.GetByteSubArray((int)ExtensionDataStartAddress, 4, true), 0);
                ExtensionData = new ExtensionData(data.GetByteSubArray((int)ExtensionDataStartAddress, len));
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(TypeIndicator.TobyteArray());
            result.AddRange(VersionNumber.TobyteArray());
            result.AddRange(PlayListStartAddress.GetBytes(true));
            result.AddRange(PlayListMarkStartAddress.GetBytes(true));
            result.AddRange(ExtensionDataStartAddress.GetBytes(true));
            result.AddRange(RESERVED_1);
            result.AddRange(AppInfoPlayList.ToRawData());
            result.AddRange(PlayList.ToRawData());
            result.AddRange(PlayListMark.ToRawData());
            if (ExtensionDataStartAddress != 0)
                result.AddRange(ExtensionData.ToRawData());

            return result.ToArray();
        }

        public Mpls Copy()
        {
            return new Mpls(this.ToRawData());
        }

        public Mpls[] Split()
        {
            Mpls copy = this.Copy();
            
            int len = 6;
            foreach (PlayItem pi in copy.PlayList.PlayItems)
                len += pi.SIZE;

            foreach (SubPath sp in copy.PlayList.SubPaths)
                len += sp.SIZE;

            // Copy PlayItems to separate array for safekeeping
            PlayItem[] playItems = new PlayItem[copy.PlayList.PlayItems.Count];
            copy.PlayList.PlayItems.CopyTo(playItems);
            PlayListMarkItem[] playListMarkItems = new PlayListMarkItem[copy.PlayListMark.PlayListMarks.Count];
            copy.PlayListMark.PlayListMarks.CopyTo(playListMarkItems);

            List<Mpls> playlists = new List<Mpls>();

            for (int i = 0; i < playItems.Length; i++)
            {
                copy.PlayList.PlayItems.Clear();
                copy.PlayList.PlayItems.Add(playItems[i]);
                copy.PlayList.NumberOfPlayItems = 1;
                copy.PlayList.NumberOfSubPaths = 0;
                copy.PlayList.Length = (UInt32)(6 + playItems[i].SIZE);
                copy.PlayListMarkStartAddress = (UInt32)(copy.PlayListStartAddress + copy.PlayList.SIZE);

                copy.PlayListMark.PlayListMarks.Clear();
                foreach (PlayListMarkItem plmi in playListMarkItems)
                    if (plmi.RefToPlayItemID == i)
                        copy.PlayListMark.PlayListMarks.Add(plmi);

                foreach (PlayListMarkItem plmi in copy.PlayListMark.PlayListMarks)
                    plmi.RefToPlayItemID = 0;

                copy.PlayListMark.NumberOfPlayListMarks = (UInt16)copy.PlayListMark.PlayListMarks.Count;
                copy.PlayListMark.Length = (UInt32)(copy.PlayListMark.NumberOfPlayListMarks * PlayListMarkItem.SIZE) + 2;

                playlists.Add(copy.Copy());
            }

            return playlists.ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class ExtDataEntry : IbyteArraySerializable
    {
        public const int SIZE = 0xC;
        public UInt16 ExtDataType { get; set; }
        public UInt16 ExtDataVersion { get; set; }
        public UInt32 ExtDataStartAddress { get; set; }
        public UInt32 ExtDataLength { get; set; }

        public ExtDataEntry(byte[] data)
        {
            ExtDataType = BitConverter.ToUInt16(data.GetByteSubArray(0x0, 2, true), 0);
            ExtDataVersion = BitConverter.ToUInt16(data.GetByteSubArray(0x2, 2, true), 0);
            ExtDataStartAddress = BitConverter.ToUInt32(data.GetByteSubArray(0x4, 4, true), 0);
            ExtDataLength = BitConverter.ToUInt32(data.GetByteSubArray(0x8, 4, true), 0);
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(ExtDataType.GetBytes(true));
            result.AddRange(ExtDataVersion.GetBytes(true));
            result.AddRange(ExtDataStartAddress.GetBytes(true));
            result.AddRange(ExtDataLength.GetBytes(true));

            return result.ToArray();
        }
    }
}

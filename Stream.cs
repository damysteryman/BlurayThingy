﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class Stream
    {
        public StreamEntry Entry { get; set; }
        public StreamAttributes Attributes { get; set; }

        public Stream(StreamEntry entry, StreamAttributes attributes)
        {
            Entry = entry;
            Attributes = attributes;
        }
    }
}

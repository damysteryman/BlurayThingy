﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class ExtensionData :IbyteArraySerializable
    {
        public UInt32 Length { get; set; }
        public UInt32 DataBlockStartAddress { get; set; }
        public byte[] RESERVED_1 { get; set; }
        public byte NumberOfExtDataEntries { get; set; }
        public List<ExtDataEntry> ExDataEntries { get; set; }
        public byte[] ExtData { get; set; }

        public ExtensionData(byte[] data)
        {
            Length = 0;
        }

        public byte[] ToRawData()
        {
            throw new NotImplementedException();
            //return BitConverter.Getbytes(Length);
        }
    }
}

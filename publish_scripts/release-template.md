# DOWNLOAD {CI_COMMIT_TAG}

*Requires .NET 5.0+*

You can add the BlurayThingy NuGet package repo as a package source to your project's NuGet.Config file and add the package reference to your project, or you can download the .nupkg manually. 

## Adding BlurayThingy package repo to your project

Create/edit a file named NuGet.Config in your project's root directory, and ensure the following packageSource data is added to it:

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSources>
    <add key="gitlab" value="https://gitlab.com/api/v4/projects/21772148/packages/nuget/index.json" />
  </packageSources>
  <packageSourceCredentials>
     <gitlab>
         <add key="Username" value="bluraythingy-dt-ro" />
         <add key="ClearTextPassword" value="QsAKgTGDM4zy9H7XEudA" />
     </gitlab>
 </packageSourceCredentials>
</configuration>
```
Then add a package reference to BlurayThingy to your project. This can be done via CLI with the following command:

```dotnet add <your-project.csproj> package BlurayThingy -v {CI_COMMIT_TAG}```

## Manual Download
[Download {CI_COMMIT_TAG} NuGet Package here! (via GitLab Package Registry)]({DOWNLOAD_PAGE})

# CHANGELOG
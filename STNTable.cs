﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class STNTable : IbyteArraySerializable
    {
        public int SIZE => Length + 2;
        public UInt16 Length { get; set; }
        public byte[] RESERVED_1 { get; set; }
        public byte NumberOfPrimaryVideoStreamEntries { get; set; }
        public byte NumberOfPrimaryAudioStreamEntries { get; set; }
        public byte NumberOfPrimaryPGStreamEntries { get; set; }
        public byte NumberOfPrimaryIGStreamEntries { get; set; }
        public byte NumberOfSecondaryAudioStreamEntries { get; set; }
        public byte NumberOfSecondaryVideoStreamEntries { get; set; }
        public byte NumberOfSecondaryPGStreamEntries { get; set; }
        public byte[] RESERVED_2 { get; set; }
        public List<Stream> PrimaryVideoStreams { get; set; }
        public List<Stream> PrimaryAudioStreams { get; set; }
        public List<Stream> PrimaryPGStreams { get; set; }
        public List<Stream> SecondaryPGStreams { get; set; }
        public List<Stream> PrimaryIGStreams { get; set; }
        public List<Stream> SecondaryVideoStreams { get; set; }
        public List<Stream> SecondaryAudioStreams { get; set; }

        public STNTable(byte[] data)
        {
            Length = BitConverter.ToUInt16(data.GetByteSubArray(0x0, 2, true), 0);
            RESERVED_1 = data.GetByteSubArray(0x2, 2);
            NumberOfPrimaryVideoStreamEntries = data[0x4];
            NumberOfPrimaryAudioStreamEntries = data[0x5];
            NumberOfPrimaryPGStreamEntries = data[0x6];
            NumberOfPrimaryIGStreamEntries = data[0x7];
            NumberOfSecondaryAudioStreamEntries = data[0x8];
            NumberOfSecondaryVideoStreamEntries = data[0x9];
            NumberOfSecondaryPGStreamEntries = data[0xA];
            RESERVED_2 = data.GetByteSubArray(0xB, 5);

            int offset = 0x10;

            PrimaryVideoStreams = new List<Stream>();
            for (int i = 0; i < NumberOfPrimaryVideoStreamEntries; i++)
            {
                StreamEntry strEntry = new StreamEntry(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strEntry.SIZE;
                StreamAttributes strAttr = new StreamAttributes(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strAttr.SIZE;

                PrimaryVideoStreams.Add(new Stream(strEntry, strAttr));
            }

            PrimaryAudioStreams = new List<Stream>();
            for (int i = 0; i < NumberOfPrimaryAudioStreamEntries; i++)
            {
                StreamEntry strEntry = new StreamEntry(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strEntry.SIZE;
                StreamAttributes strAttr = new StreamAttributes(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strAttr.SIZE;

                PrimaryAudioStreams.Add(new Stream(strEntry, strAttr));
            }

            PrimaryPGStreams = new List<Stream>();
            for (int i = 0; i < NumberOfPrimaryPGStreamEntries; i++)
            {
                StreamEntry strEntry = new StreamEntry(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strEntry.SIZE;
                StreamAttributes strAttr = new StreamAttributes(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strAttr.SIZE;

                PrimaryPGStreams.Add(new Stream(strEntry, strAttr));
            }

            SecondaryPGStreams = new List<Stream>();
            for (int i = 0; i < NumberOfSecondaryPGStreamEntries; i++)
            {
                StreamEntry strEntry = new StreamEntry(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strEntry.SIZE;
                StreamAttributes strAttr = new StreamAttributes(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strAttr.SIZE;

                SecondaryPGStreams.Add(new Stream(strEntry, strAttr));
            }

            PrimaryIGStreams = new List<Stream>();
            for (int i = 0; i < NumberOfPrimaryIGStreamEntries; i++)
            {
                StreamEntry strEntry = new StreamEntry(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strEntry.SIZE;
                StreamAttributes strAttr = new StreamAttributes(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strAttr.SIZE;

                PrimaryIGStreams.Add(new Stream(strEntry, strAttr));
            }

            SecondaryAudioStreams = new List<Stream>();
            for (int i = 0; i < NumberOfSecondaryAudioStreamEntries; i++)
            {
                StreamEntry strEntry = new StreamEntry(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strEntry.SIZE;
                StreamAttributes strAttr = new StreamAttributes(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strAttr.SIZE;

                SecondaryAudioStreams.Add(new Stream(strEntry, strAttr));
            }

            SecondaryVideoStreams = new List<Stream>();
            for (int i = 0; i < NumberOfSecondaryVideoStreamEntries; i++)
            {
                StreamEntry strEntry = new StreamEntry(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strEntry.SIZE;
                StreamAttributes strAttr = new StreamAttributes(data.GetByteSubArray(offset, data[offset] + 1));
                offset += strAttr.SIZE;

                SecondaryVideoStreams.Add(new Stream(strEntry, strAttr));
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Length.GetBytes(true));
            result.AddRange(RESERVED_1);
            result.Add(NumberOfPrimaryVideoStreamEntries);
            result.Add(NumberOfPrimaryAudioStreamEntries);
            result.Add(NumberOfPrimaryPGStreamEntries);
            result.Add(NumberOfPrimaryIGStreamEntries);
            result.Add(NumberOfSecondaryAudioStreamEntries);
            result.Add(NumberOfSecondaryVideoStreamEntries);
            result.Add(NumberOfSecondaryPGStreamEntries);
            result.AddRange(RESERVED_2);

            foreach (Stream s in PrimaryVideoStreams)
            {
                result.AddRange(s.Entry.ToRawData());
                result.AddRange(s.Attributes.ToRawData());
            }

            foreach (Stream s in PrimaryAudioStreams)
            {
                result.AddRange(s.Entry.ToRawData());
                result.AddRange(s.Attributes.ToRawData());
            }

            foreach (Stream s in PrimaryPGStreams)
            {
                result.AddRange(s.Entry.ToRawData());
                result.AddRange(s.Attributes.ToRawData());
            }

            foreach (Stream s in SecondaryPGStreams)
            {
                result.AddRange(s.Entry.ToRawData());
                result.AddRange(s.Attributes.ToRawData());
            }

            foreach (Stream s in PrimaryIGStreams)
            {
                result.AddRange(s.Entry.ToRawData());
                result.AddRange(s.Attributes.ToRawData());
            }

            foreach (Stream s in SecondaryAudioStreams)
            {
                result.AddRange(s.Entry.ToRawData());
                result.AddRange(s.Attributes.ToRawData());
            }

            foreach (Stream s in SecondaryVideoStreams)
            {
                result.AddRange(s.Entry.ToRawData());
                result.AddRange(s.Attributes.ToRawData());
            }

            return result.ToArray();
        }
    }
}

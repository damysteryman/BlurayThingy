﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class SubPath : IbyteArraySerializable
    {
        public int SIZE => (int)Length + 4;
        public UInt32 Length { get; set; }
        public byte RESERVED_1 { get; set; }
        public byte SubPathType { get; set; }
        public UInt16 RESERVED_2 { get; set; }
        public bool IsRepeatSubPath { get; set; }
        public UInt16 NumberOfSubPlayItems { get; set; }
        public List<SubPlayItem> SubPlayItems { get; set; }

        public SubPath(byte[] data)
        {
            Length = BitConverter.ToUInt32(data.GetByteSubArray(0x0, 4, true), 0);
            RESERVED_1 = data[0x4];
            SubPathType = data[0x5];
            UInt16 temp = BitConverter.ToUInt16(data.GetByteSubArray(0x6, 2, true), 0);
            RESERVED_2 = (UInt16)(temp >> 1);
            IsRepeatSubPath = (temp & 1) == 1;
            NumberOfSubPlayItems = BitConverter.ToUInt16(data.GetByteSubArray(0x8, 2, true), 0);

            int offset = 0xA;
            SubPlayItems = new List<SubPlayItem>();
            for (int i = 0; i < NumberOfSubPlayItems; i++)
            {
                int size = 2 + (BitConverter.ToUInt16(data.GetByteSubArray(offset, 2, true), 0));
                SubPlayItems.Add(new SubPlayItem(data.GetByteSubArray(offset, size)));
                offset += size;
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Length.GetBytes(true));
            result.Add(RESERVED_1);
            result.Add(SubPathType);
            UInt16 temp = (UInt16)((RESERVED_2 << 1) + (IsRepeatSubPath ? 1 : 0));
            result.AddRange(temp.GetBytes(true));
            result.AddRange(NumberOfSubPlayItems.GetBytes(true));
            foreach (SubPlayItem spi in SubPlayItems)
                result.AddRange(spi.ToRawData());

            return result.ToArray();
        }
    }
}

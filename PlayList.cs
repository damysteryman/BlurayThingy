﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class PlayList : IbyteArraySerializable
    {
        public int SIZE => (int)Length + 4;
        public UInt32 Length { get; set; }
        public byte[] RESERVED { get; set; }
        public UInt16 NumberOfPlayItems { get; set; }
        public UInt16 NumberOfSubPaths { get; set; }
        public List<PlayItem> PlayItems { get; set; }
        public List<SubPath> SubPaths { get; set; }

        public PlayList(byte[] data)
        {
            Length = BitConverter.ToUInt32(data.GetByteSubArray(0x0, 4, true), 0);
            RESERVED = data.GetByteSubArray(0x4, 2);
            NumberOfPlayItems = BitConverter.ToUInt16(data.GetByteSubArray(0x6, 2, true), 0);
            NumberOfSubPaths = BitConverter.ToUInt16(data.GetByteSubArray(0x8, 2, true), 0);

            int offset = 0xA;
            PlayItems = new List<PlayItem>();
            for (int i = 0; i < NumberOfPlayItems; i++)
            {
                int len = 2 + BitConverter.ToUInt16(data.GetByteSubArray(offset, 2, true), 0);
                PlayItems.Add(new PlayItem(data.GetByteSubArray(offset, len)));
                offset += len;
            }

            SubPaths = new List<SubPath>();
            for (int i = 0; i < NumberOfSubPaths; i++)
            {
                int len = (int)(4 + BitConverter.ToUInt32(data.GetByteSubArray(offset, 4, true), 0));
                SubPaths.Add(new SubPath(data.GetByteSubArray(offset, len)));
                offset += len;
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Length.GetBytes(true));
            result.AddRange(RESERVED);
            result.AddRange(NumberOfPlayItems.GetBytes(true));
            result.AddRange(NumberOfSubPaths.GetBytes(true));

            if (NumberOfPlayItems > 0)
                foreach (PlayItem pi in PlayItems)
                    result.AddRange(pi.ToRawData());

            if (NumberOfSubPaths > 0)
                foreach (SubPath sp in SubPaths)
                    result.AddRange(sp.ToRawData());

            return result.ToArray();
        }
    }
}

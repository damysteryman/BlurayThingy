﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class SequenceInfo : IbyteArraySerializable
    {
        public UInt32 Length { get; set; }
        public byte RESERVED { get; set; }
        public byte ATCSequencesCount { get; set; }
        public List<ATCSequence> ATCSequences { get; set; }
        public int SIZE
        {
            get
            {
                int s = 6;
                foreach (ATCSequence seq in ATCSequences)
                    s += seq.SIZE;

                return s;
            }
        }
        public byte[] ToRawData()
        {
            throw new NotImplementedException();
        }
    }

    public class ATCSequence : IbyteArraySerializable
    {
        public UInt32 SPNATCStart { get; set; }
        public byte STCSequencesCount { get; set; }
        public byte OffsetSTCID { get; set; }
        public List<STCSequence> STCSequences { get; set; }
        public int SIZE
        {
            get
            {
                return 6 + (STCSequence.SIZE * STCSequencesCount);
            }
        }

        public ATCSequence(byte[] data)
        {
            SPNATCStart = BitConverter.ToUInt32(data.GetByteSubArray(0, 4), 0);
            STCSequencesCount = data[4];
            OffsetSTCID = data[5];

            for (int i = 0; i < STCSequencesCount; i++)
                STCSequences.Add(new STCSequence(data.GetByteSubArray(6 + (STCSequence.SIZE * i), STCSequence.SIZE)));
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(SPNATCStart.GetBytes());
            result.Add(STCSequencesCount);
            result.Add(OffsetSTCID);

            foreach (STCSequence seq in STCSequences)
                result.AddRange(seq.ToRawData());

            return result.ToArray();
        }
    }

    public class STCSequence : IbyteArraySerializable
    {
        public const int SIZE = 14;
        public UInt16 PCRPID { get; set; }
        public UInt32 SPNSTCStart { get; set; }
        public UInt32 PresentationStartTime { get; set; }
        public UInt32 PresentationEndTime { get; set; }

        public STCSequence(byte[] data)
        {
            PCRPID = BitConverter.ToUInt16(data.GetByteSubArray(0, 2, true), 0);
            SPNSTCStart = BitConverter.ToUInt32(data.GetByteSubArray(2, 4, true), 0);
            PresentationStartTime = BitConverter.ToUInt32(data.GetByteSubArray(6, 4, true), 0);
            PresentationEndTime = BitConverter.ToUInt32(data.GetByteSubArray(10, 4, true), 0);
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(PCRPID.GetBytes(true));
            result.AddRange(SPNSTCStart.GetBytes(true));
            result.AddRange(PresentationStartTime.GetBytes(true));
            result.AddRange(PresentationEndTime.GetBytes(true));

            return result.ToArray();
        }
    }
}

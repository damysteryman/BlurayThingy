﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class PlayListMarkItem : IbyteArraySerializable
    {
        public const int SIZE = 0xE;
        public byte RESERVED { get; set; }
        public byte MarkType { get; set; }
        public UInt16 RefToPlayItemID { get; set; }
        public UInt32 MarkTimeStamp { get; set; }
        public UInt16 EntryESPID { get; set; }
        public UInt32 Duration { get; set; }

        public PlayListMarkItem(byte[] data)
        {
            RESERVED = data[0];
            MarkType = data[1];
            RefToPlayItemID = BitConverter.ToUInt16(data.GetByteSubArray(0x2, 2, true), 0);
            MarkTimeStamp = BitConverter.ToUInt32(data.GetByteSubArray(0x4, 4, true), 0);
            EntryESPID = BitConverter.ToUInt16(data.GetByteSubArray(0x8, 2, true), 0);
            Duration = BitConverter.ToUInt32(data.GetByteSubArray(0xA, 4, true), 0);
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.Add(RESERVED);
            result.Add(MarkType);
            result.AddRange(RefToPlayItemID.GetBytes(true));
            result.AddRange(MarkTimeStamp.GetBytes(true));
            result.AddRange(EntryESPID.GetBytes(true));
            result.AddRange(Duration.GetBytes(true));

            return result.ToArray();
        }
    }
}

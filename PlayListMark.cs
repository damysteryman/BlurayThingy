﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class PlayListMark : IbyteArraySerializable
    {
        public UInt32 Length { get; set; }
        public UInt16 NumberOfPlayListMarks { get; set; }
        public List<PlayListMarkItem> PlayListMarks { get; set; }

        public PlayListMark(byte[] data)
        {
            Length = BitConverter.ToUInt32(data.GetByteSubArray(0, 4, true), 0);
            NumberOfPlayListMarks = BitConverter.ToUInt16(data.GetByteSubArray(4, 2, true), 0);

            PlayListMarks = new List<PlayListMarkItem>();
            for (int i = 0; i < NumberOfPlayListMarks; i++)
                PlayListMarks.Add(new PlayListMarkItem(data.GetByteSubArray(0x6 + (i * PlayListMarkItem.SIZE), PlayListMarkItem.SIZE)));
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange(Length.GetBytes());
            result.AddRange(NumberOfPlayListMarks.GetBytes());

            foreach (PlayListMarkItem plmi in PlayListMarks)
                result.AddRange(plmi.ToRawData());

            return result.ToArray();
        }
    }
}

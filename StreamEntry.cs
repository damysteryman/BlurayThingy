﻿using System;
using System.Collections.Generic;

namespace BlurayThingy
{
    public class StreamEntry : IbyteArraySerializable
    {
        public int SIZE => Length + 1;
        public byte Length { get; set; }
        public byte StreamType { get; set; }
        public byte RefToSubPathID { get; set; }
        public byte RefToSubClipID { get; set; }
        public UInt16 RefToStreamPID { get; set; }

        public StreamEntry(byte[] data)
        {
            Length = data[0x0];
            StreamType = data[0x1];

            switch (StreamType)
            {
                case 2:
                case 4:
                    RefToSubPathID = data[0x2];
                    RefToSubClipID = data[0x3];
                    RefToStreamPID = BitConverter.ToUInt16(data.GetByteSubArray(0x4, 2, true), 0);
                    break;

                default:
                    RefToStreamPID = BitConverter.ToUInt16(data.GetByteSubArray(0x2, 2, true), 0);
                    break;
            }
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.Add(Length);
            result.Add(StreamType);

            switch (StreamType)
            {
                case 2:
                case 4:
                    result.Add(RefToSubPathID);
                    result.Add(RefToSubClipID);
                    break;
            }

            result.AddRange(RefToStreamPID.GetBytes(true));

            // pad out to correct size
            while (result.Count < SIZE)
                result.Add(0);

            return result.ToArray();
        }
    }
}

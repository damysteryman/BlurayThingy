﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlurayThingy
{
    public class Clpi : IbyteArraySerializable
    {
        public string TypeIndicator { get; set; }
        public string VersionNumber { get; set; }
        public UInt32 SequenceInfoStartAddress { get; set; }
        public UInt32 ProgramInfoStartAddress { get; set; }
        public UInt32 CPIStartAddress { get; set; }
        public UInt32 ClipMarkStartAddress { get; set; }
        public UInt32 ExtensionDataStartAddress { get; set; }
        public byte[] RESERVED { get; set; }


        public byte[] ToRawData()
        {
            throw new NotImplementedException();
        }
    }
}
